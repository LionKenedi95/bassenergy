<?php
	header("Content-Type: text/html; charset=utf-8");
	session_start();
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link rel="shortcut icon" href="favicon.png">
	<meta name="robots" content="noindex">
	
    <meta name="author" content="Вадим Демченко (https://vk.com/lionkenedi95)">

	<title>Admin BassEnergy</title>



	<link rel="stylesheet" type="text/css" href="../css/default.css">
	<link rel="stylesheet" type="text/css" href="../css/topnav/display.css">
	<link rel="stylesheet" type="text/css" href="../css/topnav/style.css">
	<link rel="stylesheet" type="text/css" href="../css/footer/display.css">
	<link rel="stylesheet" type="text/css" href="../css/footer/style.css">
</head>
<body>
<div class='white-body'>
	<div class="admin-panel">
		<span class='space-span space-span-50'></span>
		<h2>Панель администратора</h2>
		<?php 
			if ($_SESSION['loginset'] == 1) {
				include_once("snap.php");
			}
			else{
				$errorspan = '';
				if ($_SESSION['loginset'] == 2) {
					$errorspan = "<span class='login-panel__span login-panel__span_error'>Неправильный логин или пароль</span>";
				}
				echo "
					<div class='login-panel'>
						<form method='POST' action='login.php' class='login-panel__box'>
							<h3 class='login-panel__header'>Авторизуйтесь в панели администратора</h3> " . $errorspan . "
							<span class='login-panel__span'>Логин: </span>
							<input type='text' name='login' class='login-panel__input'>
							<span class='login-panel__span'>Пароль: </span>
							<input type='password' name='pass' class='login-panel__input'>
							<span class='login-panel__span'><input type='submit' class='login-panel__submit'></span>

						</form>
					</div>
				";
			}
		?>
	</div>
</div>
	<footer class='footer'>
		<div class='footer__wrap'>
			<h5 class='footer__desc'>Официальный диллер автоаудиотехники в Ростове-на-Дону и Ростовской области - "магазин автозвука "BASS ENERGY".<br>Автомобильные сабвуферы, усилители, а также автомобильная звукоизоляция и комплектующие к автоаудиооборудованию.<br>2016г. ИП Илющихин Д.С.</h5>
			<div class='footer__contact'>
				<a href="#" class='footer_contact-link'>+7 (999) 698-65-00</a>
				<a href="#" class='footer_contact-link'>shop@bassenergy.ru</a>
			</div>
		</div>
		<h3 class='footer__logo'>BASS ENERGY</h3>
	</footer>
	<nav class='top-nav'>
		<div class='top-nav__content'>
			<a href="http://bassenergy.ru/index.html" class='top-nav__link'>Вернуться на сайт</a>
			<?php 
			if ($_SESSION['loginset'] == 1) {
				echo "<a href='logout.php' class='top-nav__link'>Выйти</a>";
			}
			?>
		</div>
	</nav>
	<a href="#top" id='link-pageup' class='link-pageup-hidden'></a>
	<!-- JS link -->
	<script src='../js/main.js'></script>

	<link rel="stylesheet" type="text/css" href="../css/admin/display.css">
	<link rel="stylesheet" type="text/css" href="../css/admin/style.css">
</body>
</html>