
	<div class='admin-panel__nav'>
	<a href="#" class='admin-panel__nav-link' id='admin-panel__nav-link-create'>Создать товар</a>
	<a href="#" class='admin-panel__nav-link'>Редактировать товар</a>
	<a href="#" class='admin-panel__nav-link'>Удалить товар</a>
	</div>

	<div class="create-panel" id='create-panel'>
		<div class="create-panel__box"  id='create-panel__box'>
			<p>Введите данные:</p>
			<div class='create-panel__box-item'><span class='create-panel__span'>Название</span><input type="text"id='input_product_name'></div>
			<div class='create-panel__box-item'><span class='create-panel__span'>Краткое описание</span><textarea id='input_product_desc-min'></textarea></div>
			<div class='create-panel__box-item'><span class='create-panel__span'>Полное описание</span><textarea id='input_product_desc-full'></textarea></div>
			<div class='create-panel__box-item'><span class='create-panel__span'>Цена</span><input type="text" id='input_product_price'></div>
			<p>Выберите тип товара:</p>
			<div class='create-panel__box-item'>
				<select id='create-panel__select-type' class='create-panel__select'>
					<option disabled>Выберите тип товара</option>
					<!-- <option product_type='new'>Новый тип товара</option> -->
				</select>
				<button id='btn-load-param'>Загрузить параметры</button>
			</div>
		</div>
		<a id='create-panel__create-link' class='create-panel__create-link_hidden'>Создать товар</a>
	</div>
	<script src='../js/adminpanel.js'></script>
	<link rel="stylesheet" type="text/css" href="../css/admin/createpanel.css">