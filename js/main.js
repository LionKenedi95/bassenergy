console.log('main.js load!');

//инциалзация переменных input'ов
	var clientName = document.getElementById('client__name');
	var clientPhone = document.getElementById('client__phone');
	var clientEmail = document.getElementById('client__email');
	var clientMessage = document.getElementById('client__message');

//проверка волидности формы Contact
function getVerificationContact() {
	var answer = true; // изначально считаем, что все ок

	var re = /^[а-яa-z]/i;
	var valid = re.test(clientName.value);
	var tips = document.getElementById(clientName.getAttribute('tipserror'));
	if (valid) {
	   	tips.className = 'contact__tips-error';
	   	clientName.className += ' contact__message__input_success';
	}
	else{
	   	tips.className = 'contact__tips-error_visible';
	   	clientName.className = 'contact__message__input';
		answer = false;
	};

	re = /^\d[\d\(\)\ -]{4,14}\d$/;
	valid = re.test(clientPhone.value);
	tips = document.getElementById(clientPhone.getAttribute('tipserror'));
	if (valid) {
		tips.className = 'contact__tips-error';
	clientPhone.className += ' contact__message__input_success';
	}
	else{
	    tips.className = 'contact__tips-error_visible';
	    clientPhone.className = 'contact__message__input';
	    answer = false;
	};

	re = /^[\w-\.]+@[\w-]+\.[a-z]{2,4}$/i;
	valid = re.test(clientEmail.value);
	tips = document.getElementById(clientEmail.getAttribute('tipserror'));
	if (clientEmail.value != '') {
		if (valid) {
			tips.className = 'contact__tips-error';
			clientEmail.className += ' contact__message__input_success';
		}
		else{
			tips.className = 'contact__tips-error_visible';
			clientEmail.className = 'contact__message__input';
			answer = false;
		};
	}
	return answer;
}

// Отправка формы Contact
document.getElementById('contact__submit-link').onclick = function() {
	if (getVerificationContact()) {
		console.log('run function sendMail')
		// (1) создать объект для запроса к серверу
		var req = new XMLHttpRequest(); 
	    // (2)
		// span рядом с кнопкой
		// в нем будем отображать ход выполнения
		//var statusElem = document.getElementById('vote_status') 
		
		req.onreadystatechange = function() {  
	        // onreadystatechange активируется при получении ответа сервера

			if (req.readyState == 4) { 
	            // если запрос закончил выполняться

				//statusElem.innerHTML = req.statusText // показать статус (Not Found, ОК..)

				if(req.status == 200) { 
	                 // если статус 200 (ОК) - выдать ответ пользователю
					if(req.responseText = 'true'){
						console.log('Сообщение успешно обработанно');
						document.getElementById('contact__answer-send').innerHTML = 'Сообщение успешно передано!';
						document.getElementById('contact__answer-send').className += ' contact__answer-send_success';
					}
					else {
						console.log('Ошибка: невалидное сообщение');
					}
				}
				else{
					console.log('Ошибка при передачи сообщения');
					document.getElementById('contact__answer-send').innerHTML = 'Ошибка при передачи сообщения.<br>Перезагрузите страницу :(';
					document.getElementById('contact__answer-send').className += ' contact__answer-send_error';
				}
				// тут можно добавить else с обработкой ошибок запроса
			}
		}

	       // (3) задать адрес подключения
		req.open('POST', 'php/sendmail.php', true);  
		req.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded'); // Отправляем кодировку
		// объект запроса подготовлен: указан адрес и создана функция onreadystatechange
		// для обработки ответа сервера
		 
	        // (4)
	    var sendtext = 'name=' + clientName.value + '&tel=' + clientPhone.value;
	    if (clientEmail.value != '') {sendtext += '&email=' + clientEmail.value;};
	    if (clientMessage.value != '') {sendtext += '&message=' + clientMessage.value;};
		req.send(sendtext);  // отослать запрос
	  
	        // (5)
		//statusElem.innerHTML = 'Ожидаю ответа сервера...' 
	}
}

//Отслеживание прокрутки страницы для кнопки "вверх"
var linkPageUp = document.getElementById('link-pageup');
window.onscroll = function() {
	if (window.pageYOffset > document.documentElement.clientHeight) {
		linkPageUp.style.display = 'block';
	}
	else {
		linkPageUp.style.display = 'none';
	}
}
