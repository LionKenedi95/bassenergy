var linkCreate = document.getElementById('admin-panel__nav-link-create');
var panelCreate = document.getElementById('create-panel');
var answer = 0;
linkCreate.onclick = function() {
	panelCreate.className += ' create-panel_visible';
	// Запрос в базу для получения списка типов товара
	var req = new XMLHttpRequest(); 
	    // (2)
		// span рядом с кнопкой
		// в нем будем отображать ход выполнения
		//var statusElem = document.getElementById('vote_status') 
		
	req.onreadystatechange = function() {  
	    // onreadystatechange активируется при получении ответа сервера

		if (req.readyState == 4) { 
	    // если запрос закончил выполняться
	    //statusElem.innerHTML = req.statusText // показать статус (Not Found, ОК..)
			if(req.status == 200) { 
				console.log('Сообщение пришло');
				answer = JSON.parse(req.responseText);

				// создаем список в select'е
				var selectParam = document.getElementById('create-panel__select-type');
				for (var i = 0; i < answer.length ; i++) {
					var newoption = document.createElement('option');
  					selectParam.appendChild(newoption);
  					newoption.innerHTML = answer[i].product_type_name;
  					newoption.setAttribute('product_type', answer[i].product_type_id);
				}

			}
			else{
				console.log('Ошибка при передачи сообщения');
			}
		// тут можно добавить else с обработкой ошибок запроса
		}
	}
	// (3) задать адрес подключения
	req.open('POST', '/php/create.php', true);  
	req.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded'); // Отправляем кодировку
	// объект запроса подготовлен: указан адрес и создана функция onreadystatechange
	// для обработки ответа сервера
	// (4)
	var sendtext = 'comand=licreate';
	req.send(sendtext); 

}

var btnLoadParam = document.getElementById('btn-load-param');
btnLoadParam.onclick = function() {
	// Запрос в базу для получения списка параметров товара
	var req1 = new XMLHttpRequest(); 
	    // (2)
		// span рядом с кнопкой
		// в нем будем отображать ход выполнения
		//var statusElem = document.getElementById('vote_status') 
		
	req1.onreadystatechange = function() {  
	    // onreadystatechange активируется при получении ответа сервера

		if (req1.readyState == 4) { 
	    // если запрос закончил выполняться
	    //statusElem.innerHTML = req.statusText // показать статус (Not Found, ОК..)
			if(req1.status == 200) { 
				console.log('Сообщение пришло. Ответ: ' + req1.responseText);
				answer = JSON.parse(req1.responseText);

				//создаем <div class='create-panel__box-item'><span>Цена: </span><input type="text" name="name"></div>
				var createPanelBox = document.getElementById('create-panel__box');
				for (var i = 0; i < answer.length ; i++) {
					var newdiv = document.createElement('div');
					newdiv.className = 'create-panel__box-item';
					var newspan = document.createElement('span');
					newspan.innerHTML = answer[i].param_name;
					newspan.className = 'create-panel__span';
					var newinput = document.createElement('input');
					newinput.setAttribute('type', 'text');
					newinput.setAttribute('param_id', answer[i].param_id);
					newinput.className = 'create-panel__input';
					newdiv.appendChild(newspan);
					newdiv.appendChild(newinput);
  					createPanelBox.appendChild(newdiv);
				}
				document.getElementById('create-panel__create-link').className = 'create-panel__create-link_visibled';		
			}
			else{
				console.log('Ошибка при передачи сообщения');
			}
		// тут можно добавить else с обработкой ошибок запроса
		}
	}
	// (3) задать адрес подключения
	req1.open('POST', '/php/create.php', true);  
	req1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded'); // Отправляем кодировку
	// объект запроса подготовлен: указан адрес и создана функция onreadystatechange
	// для обработки ответа сервера
	// (4)
	var selectParam = document.getElementById("create-panel__select-type"); // Получаем наш список
	var val = selectParam.options[selectParam.selectedIndex].getAttribute('product_type'); 
	var sendtext = 'id=' + val;
	req1.send(sendtext); 

}

var linkCreateNewProduct = document.getElementById('create-panel__create-link');
linkCreateNewProduct.onclick = function() {
	// Запрос в базу для получения списка параметров товара
	var req2 = new XMLHttpRequest(); 
	    // (2)
		// span рядом с кнопкой
		// в нем будем отображать ход выполнения
		//var statusElem = document.getElementById('vote_status') 
		
	req2.onreadystatechange = function() {  
	    // onreadystatechange активируется при получении ответа сервера

		if (req2.readyState == 4) { 
	    // если запрос закончил выполняться
	    //statusElem.innerHTML = req.statusText // показать статус (Not Found, ОК..)
			if(req2.status == 200) { 
				if (req2.responseText != '') {
					alert('Новый товар успешно создан!');
					var createPanelBox = document.getElementById('create-panel__box');
					while (createPanelBox.lastChild){
						createPanelBox.removeChild(createPanelBox.lastChild);
					}
					var newform = document.createElement('form');
					newform.setAttribute('method','POST');
					newform.setAttribute('action','/php/create.php');
					newform.setAttribute('enctype','multipart/form-data');
					createPanelBox.appendChild(newform);
					var newp = document.createElement('p');
					newp.innerHTML='выберите изображения для созданного товара:</br>';
					newform.appendChild(newp);
					var newinput = document.createElement('input');
					newinput.setAttribute('name','filein[]');
					newinput.setAttribute('type','file');
					newinput.setAttribute('multiple','multiple');
					newform.appendChild(newinput);
					var newsub = document.createElement('input');
					newsub.setAttribute('type','submit');
					newsub.setAttribute('value','Отправить');
					newform.appendChild(newsub);
					var newinput = document.createElement('input');
					newinput.setAttribute('name','product_name');
					newinput.setAttribute('type','hidden');
					newinput.setAttribute('value',req2.responseText);
					newform.appendChild(newinput);
					document.getElementById('create-panel__create-link').remove();
				}
			}
			else{
				console.log('Ошибка при передачи сообщения');
			}
		// тут можно добавить else с обработкой ошибок запроса
		}
	}
	// (3) задать адрес подключения
	req2.open('POST', '/php/create.php', true);  
	req2.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded'); // Отправляем кодировку
	// объект запроса подготовлен: указан адрес и создана функция onreadystatechange
	// для обработки ответа сервера
	// (4)
	// Получаем наш список
	var inputList = document.getElementById('create-panel__box').getElementsByClassName('create-panel__input'); 
	var sendtext = 'comand=createproduct&param_id=';
	for (var i = 0; i < inputList.length; i++) {
		if (i != 0) { sendtext += '|';}
		sendtext +=  inputList[i].getAttribute('param_id');
	}
	sendtext += '&param_value=';
	for (var i = 0; i < inputList.length; i++) {
		if (i != 0) { sendtext += '|';}
		sendtext +=  inputList[i].value;
	}
	sendtext += '&product_name=' + document.getElementById('input_product_name').value;
	sendtext += '&product_desc-min=' + document.getElementById('input_product_desc-min').value;
	sendtext += '&product_desc-full=' + document.getElementById('input_product_desc-full').value;
	sendtext += '&product_price=' + document.getElementById('input_product_price').value;
	sendtext += '&product_type=' + document.getElementById('create-panel__select-type').options[document.getElementById('create-panel__select-type').selectedIndex].getAttribute('product_type');
	console.log(sendtext);
	req2.send(sendtext); 

}
